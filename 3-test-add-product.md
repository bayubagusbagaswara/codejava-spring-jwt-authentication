# Add Product

- Method : POST
- URL : `localhost:8080/products`
- Authorization : Bearer
- Request Body :

```json
{
  "name": "string",
  "price": "number"
}
```
- Response Body

```json
{
  "id": "number",
  "name": "string",
  "price": "number"
}
```