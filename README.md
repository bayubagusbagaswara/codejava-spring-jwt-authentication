# Spring JWT Authentication

- Secure REST APIs with Spring Security and JSON Web Token (JWT):
  - Understand Overview of JWT
  - Understand Spring Security Filters chain
  - Code a Spring Boot application the exposes some APIs secured by JWT
  - Test protected APIs with curl

- By the end of this tutorial, you'll be able to:
  - Understand why JWT is wildly used to secure REST APIs

# Technologies

- Spring Web
- Spring Data JPA with Hibernate
- Spring Security
- Spring Validation
- Java JWT library (jjwt)
- PostgreSQL JDBC Driver

# Software programs

- Java Development Kit (JDK)
- IntelliJ
- curl

# Understand JSON Web Token (JWT)

- JWT is an open standard that defines a compact and self-contained way for securely transmitting information between parties as a JSON object
- What makes JWT widely used:
  - Compact: a JWT is a string -> can be used easily in HTTP Authorization headers and URI query parameters
  - Trustworthy: a JWT is digitally signed using a secret key or a public/private RSA key pair
  -> tokens can be verified and cannot be tampered with

# Structure of a JSON Web Token

- A JWT is a string representing a set of claims as a JSON object
- A claim is a piece of information asserted about a subject, represented as a name/value pair (claim name/claim value)

  ![Structure_JWT](img/structure-jwt.jpg)

# Understand Spring Security Filters Chain

- Diagram

  ![Filters_Chain](img/filters-chain.jpg)

- Generate JWT Access Token
- Code Validate JWT Access Token