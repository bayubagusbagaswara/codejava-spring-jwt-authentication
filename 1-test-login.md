# Login

- Method = POST
- URL = `http://localhost:8080/auth/login`
- Request Body
```json
{
  "email": "string",
  "password": "string"
}
```
- Response

```json
{
  "email": "string",
  "accessToken": "string"
}
```

# Login as Customer and Editor User

- Method : POST
- URL : `localhost:8080/auth/login`
- Request Body :

```json
{
  "email": "albert@gmail.com",
  "password": "albert123"
}
```
- Response

```json
{
  "email": "string",
  "accessToken": "string"
}
```