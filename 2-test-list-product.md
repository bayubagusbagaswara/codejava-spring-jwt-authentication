# List Product

- Method : GET
- URL : `localhost:8080/products`
- Authorization : Bearer
- Response Body

```json
[
  {
    "id": "number",
    "name": "string",
    "price": "number"
  },  
  {
    "id": "number",
    "name": "string",
    "price": "number"
  },
  {
    "id": "number",
    "name": "string",
    "price": "number"
  }
]
```