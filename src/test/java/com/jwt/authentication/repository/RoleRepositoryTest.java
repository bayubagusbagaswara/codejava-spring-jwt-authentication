package com.jwt.authentication.repository;

import com.jwt.authentication.entity.Role;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
class RoleRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    @Test
    void testCreateRoles() {
        Role admin = new Role("ROLE_ADMIN");
        Role editor = new Role("ROLE_EDITOR");
        Role customer = new Role("ROLE_CUSTOMER");

        roleRepository.saveAll(List.of(admin, editor, customer));

        long numberOfRoles = roleRepository.count();
        assertEquals(3, numberOfRoles);
    }

    @Test
    void testListRoles() {
        List<Role> roleList = roleRepository.findAll();
        Assertions.assertThat(roleList.size()).isGreaterThan(0);

        roleList.forEach(System.out::println);
    }


}