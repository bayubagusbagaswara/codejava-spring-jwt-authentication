package com.jwt.authentication.repository;

import com.jwt.authentication.entity.Role;
import com.jwt.authentication.entity.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void testCreateUser() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String email = "albert@gmail.com";
        String rawPassword = "albert123";
        String encodedPassword = passwordEncoder.encode(rawPassword);
        User newUser = new User(email, encodedPassword);

        final User savedUser = userRepository.save(newUser);

        assertNotNull(savedUser);
        assertNotNull(savedUser.getId());

        Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
    }

    // user Bayu memiliki ROLE EDITOR [2]
    // user Albert memiliki ROLE EDITOR dan CUSTOMER [2, 3]

    @Test
    void testAssignRolesToUser() {
        Integer userId = 2;

        User user = userRepository.findById(userId).get();
        user.addRole(new Role(2));
        user.addRole(new Role(3));

        User updatedUser = userRepository.save(user);

        Assertions.assertThat(updatedUser.getRoles()).hasSize(2);
    }
}