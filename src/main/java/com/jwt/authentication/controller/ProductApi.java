package com.jwt.authentication.controller;

import com.jwt.authentication.entity.Product;
import com.jwt.authentication.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductApi {

    @Autowired
    ProductRepository productRepository;

    @GetMapping
    @RolesAllowed({"ROLE_CUSTOMER", "ROLE_EDITOR"})
    public List<Product> list() {
        return productRepository.findAll();
    }

    @PostMapping
    @RolesAllowed("ROLE_EDITOR")
    public ResponseEntity<Product> create(@Valid @RequestBody Product product) {
        final Product savedProduct = productRepository.save(product);
        URI productURI = URI.create("/product/" + savedProduct.getId());

        return ResponseEntity.created(productURI).body(savedProduct);
    }
}
