package com.jwt.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodejavaSpringJwtAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodejavaSpringJwtAuthenticationApplication.class, args);
	}

}
